const express = require('express')

module.exports = (server) => {
  const router = express.Router()

  server.use('/api', router)

  // router.route('/test').get((req, res, next) => {
  //   res.send('worked')
  // })
  const billingCycleService = require('../api/billingCycle/billingCycleService')

  billingCycleService.register(router, '/billing_cycles')

  const billingSummaryService = require('../api/billingSummary/billingSummaryService')
  router.route('/billing_summary').get(billingSummaryService.getSummary)
}
