## Start Mongo

```bash
sudo service mongod start
```

## Start Application


### Development

```bash
npm run dev
```

### Production

```bash
npm run production
```
