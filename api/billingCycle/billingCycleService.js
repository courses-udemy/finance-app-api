const _ = require('lodash')
const BillingCycle = require('./billingCycle')

BillingCycle.methods(['get', 'post', 'put', 'delete'])
BillingCycle.updateOptions({ new: true, runValidators: true })


parserErrors = (nodeRestfulErrors) => {
  const errors = []
  _.forIn(nodeRestfulErrors, error => errors.push(error.message))
  return errors
}
handlerErrorsOrNext = (req, res, next) => {
  const bundle = res.locals.bundle

  if (bundle.errors) {
    var errors = parserErrors(bundle.errors)
    res.status(422).json({ errors })
  }
  else {
    next()
  }
}
BillingCycle.after('post', handlerErrorsOrNext).after('put', handlerErrorsOrNext)

BillingCycle.route('count', (req, res, next) => {
  BillingCycle.count((error, value) => {
    if (error) {
      res.status(500).json({ errors: [error] })
    }
    else {
      res.json({ value })
    }
  })
})

module.exports = BillingCycle
